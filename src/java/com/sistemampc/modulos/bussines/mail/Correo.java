/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.modulos.bussines.mail;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Pablo Andres Ramirez
 */
public abstract class Correo {
    
    protected final static String HOST = "smtp.gmail.com";
    protected final static String PORT = "587";
    protected final static String REMITENTE = "gaesmpc@gmail.com";
    protected final static String CLAVE = "sistemampc";

    private Properties propiedades;
    
    protected void inicializarPropiedades() {
        propiedades = new Properties();
        propiedades.put("mail.smtp.auth", "true");
        propiedades.put("mail.smtp.starttls.enable", "true");
        propiedades.put("mail.smtp.host", HOST);
        propiedades.put("mail.smtp.port", PORT);
        propiedades.put("mail.smtp.ssl.trust", HOST);
    }

    protected Session getSession() {
        Authenticator a = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(REMITENTE, CLAVE);
            }
        };
        return Session.getInstance(propiedades, a);
    }
    
    public abstract int enviarCorreo(String asunto, String mensaje, String destinatario);
}