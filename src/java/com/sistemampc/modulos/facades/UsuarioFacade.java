/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.modulos.facades;

import com.sistemampc.modelos.Usuario;
import com.sistemampc.usuarios.controladores.utilidades.GenerarPassword;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Pablo Andres Ramirez
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "sistemampcPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    
    
    public boolean usuarioCedula(String numeroCedula){
        TypedQuery<Usuario> usuarios = getEntityManager().createNamedQuery("Usuario.findByNumeroCedula",Usuario.class)
                .setParameter("numeroCedula", numeroCedula);
        return usuarios.getResultList().size()>0;
    }
    
    public boolean usuarioCorreo(String correoElectronico){
        TypedQuery<Usuario> usuarios= getEntityManager().createNamedQuery("Usuario.findByCorreoElectronico",Usuario.class)
                .setParameter("correoElectronico", correoElectronico);
        return usuarios.getResultList().size()>0;
    }
    
    public boolean registrarUsuario(Usuario usuarioControlador){
        try {
            usuarioControlador.setClave(GenerarPassword.cifrarStringSha(usuarioControlador.getClave()));
            getEntityManager().persist(usuarioControlador);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public Usuario actualizarClave(Usuario usuarioControlador){
        try {
            usuarioControlador.setClave(GenerarPassword.cifrarStringSha(usuarioControlador.getClave()));
            getEntityManager().merge(usuarioControlador);
            return usuarioControlador;
        } catch (Exception e) {
            return null;
        }
    }
    
    public boolean esClave(String clave){
        try {
            String password = GenerarPassword.cifrarStringSha(clave);
            return getEntityManager().createNamedQuery("Usuario.findByClave",Usuario.class).setParameter("clave", password).getSingleResult()!=null;
        } catch (Exception e) {
            return false;
        }
    }
    
    public Usuario buscarUsuarioNumeroCedula(String numeroCedula){
        try{
            return getEntityManager().createNamedQuery("Usuario.findByNumeroCedula", Usuario.class)
                .setParameter("numeroCedula", numeroCedula)
                .getSingleResult();
        }catch(Exception e){
            return null;
        }
    }
}
