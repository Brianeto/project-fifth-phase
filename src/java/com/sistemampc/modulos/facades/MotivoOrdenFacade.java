/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.modulos.facades;

import com.sistemampc.modelos.MotivoOrden;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Pablo Andres Ramirez
 */
@Stateless
public class MotivoOrdenFacade extends AbstractFacade<MotivoOrden> {

    @PersistenceContext(unitName = "sistemampcPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MotivoOrdenFacade() {
        super(MotivoOrden.class);
    }
    
}
