/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.modulos.utilidades;

/**
 *
 * @author Pablo Andres Ramirez
 * @param <T>
 */
public class Controlador<T> {
    private T s;

    public T getS() {
        return s;
    }

    public void setS(T s) {
        this.s = s;
    }
    
}
