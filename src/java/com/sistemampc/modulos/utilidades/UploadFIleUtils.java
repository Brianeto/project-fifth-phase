/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.modulos.utilidades;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

/**
 *
 * @author SENA
 */
public class UploadFIleUtils {

    public static void uploadFile() {
    }

    public static String upload(Part file) {
        try {
            String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("WEB-INF/archivos");
            if (path.substring(0, path.indexOf("/build")) == null) {
                path:
                path.substring(0, path.indexOf("\\build"));
                path = path + "\\web\\WEB-INF\\archivos\\";
            } else {
                path = path.substring(0, path.indexOf("/build"));
                path = path + "/web/WEB-INF/archivos/";
            }
            String nombre = file.getSubmittedFileName();
            String pathReal = "/sistemampc/WEB-INF/archivos/" + nombre;
            path = path + nombre;
            InputStream in = file.getInputStream();
            byte[] data = new byte[in.available()];
            in.read(data);
            FileOutputStream out = new FileOutputStream(new File(path));
            out.write(data);
            in.close();
            out.close();
            return pathReal;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String uploadFile(Part file, String d) {
        try {
            String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("resources/archivos");
            path = path.substring(0, path.indexOf("\\build"));
            path = path + "\\web\\resources\\archivos\\";
            String[] h = file.getContentType().split("/");
            String pathReal = "archivos/" + d + "." + h[h.length - 1];
            System.out.println(pathReal);
            path = path + d + "." + h[h.length - 1];
            InputStream in = file.getInputStream();
            byte[] data = new byte[in.available()];
            in.read(data);
            FileOutputStream out = new FileOutputStream(new File(path));
            out.write(data);
            in.close();
            out.close();
            return pathReal;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}
