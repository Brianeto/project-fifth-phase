/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.modulos.utilidades;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Pablo Andres Ramirez
 */
public abstract class AbstractConvertidor implements Converter{

    protected String nombreControlador;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            Integer llavePrimaria = Integer.valueOf(value);
            IControlador controlador = (IControlador) context.getELContext()
                    .getELResolver()
                    .getValue(context.getELContext(), null, nombreControlador);
            return controlador.getObjectoEntidad(llavePrimaria);
        } catch (NumberFormatException e) {
            System.out.println("getAsObject");
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        try {
            if(value instanceof IEntity){
                IEntity entidad = (IEntity) value;
                return entidad.getLlavePrimaria();
            }else {
                System.out.println("No instanceof IEntity");
                return null;
            }
        } catch (Exception e) {
            System.out.println("getAsString");
            return null;
        }
    }
    
}