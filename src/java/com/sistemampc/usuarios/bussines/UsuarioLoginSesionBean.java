/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.bussines;

import com.sistemampc.modelos.Usuario;
import com.sistemampc.modulos.bussines.mail.Correo;
import com.sistemampc.modulos.bussines.mail.CorreoHtml;
import com.sistemampc.usuarios.controladores.utilidades.GenerarPassword;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author APRENDIZ
 */
@Stateful
public class UsuarioLoginSesionBean {

    @PersistenceContext(unitName = "sistemampcPU")
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioLoginSesionBean() {
    }

    public Object autenticarUsuario(String numeroCedula, String clave) {
        try {
            TypedQuery<Usuario> usuarios = getEntityManager().createNamedQuery("Usuario.findByNumeroCedula", Usuario.class).setParameter("numeroCedula", numeroCedula);
            if (usuarios.getResultList().size() > 0) {
                Usuario u = usuarios.getResultList().get(0);
                if (u.getClave().equals(GenerarPassword.cifrarStringSha(clave))) {
                    System.out.println(u);
                    return u;
                } else {
                    System.out.println("3");
                    return 3;
                }
            } else {
                System.out.println("2");
                return 2;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return 1;
        }
    }

    public int clavesIguales(Usuario u, String clave) {
        try {
            String c = GenerarPassword.cifrarStringSha(clave);
            if (u.getClave().equals(c)) {
                return 3;
            } else {
                return 2;
            }
        } catch (Exception e) {
            return 1;
        }
    }

    public Object confirmarNuevaClave(Usuario u, String nuevaClave, String claveDeConfirmacion) {
        try {
            if (nuevaClave != null) {
                if (!nuevaClave.contains(" ")) {
                    Pattern digit = Pattern.compile("[0-9]");
                    Matcher tieneNumeros = digit.matcher(nuevaClave);
                    if (nuevaClave.length() >= 8) {
                        if (tieneNumeros.find()) {
                            Pattern special = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
                            Matcher hasSpecial = special.matcher(nuevaClave);
                            if (!hasSpecial.find()) {
                                if (nuevaClave.equals(claveDeConfirmacion)) {
                                    String c = GenerarPassword.cifrarStringSha(nuevaClave);
                                    u.setClave(c);
                                    getEntityManager().merge(u);
                                    Correo correo = new CorreoHtml();
                                    correo.enviarCorreo("Cambio de contraseña", "Se ha actualizado la contraseña de su cuenta", u.getCorreoElectronico());
                                    return u;
                                } else {
                                    return 7;
                                }
                            } else {
                                return 6;
                            }
                        } else {
                            return 5;
                        }
                    } else {
                        return 4;
                    }
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } catch (Exception e) {
            return 1;
        }
    }
}
