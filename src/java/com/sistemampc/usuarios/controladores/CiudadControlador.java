/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores;

import com.sistemampc.modelos.Ciudad;
import com.sistemampc.modulos.facades.CiudadFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Pablo Andres Ramirez
 */
@Named(value = "ciudadControlador")
@RequestScoped
public class CiudadControlador implements Serializable, IControlador<Ciudad>{
    
    private static final long serialVersionUID = 3L;
    private Ciudad ciudad;
    
    @EJB private CiudadFacade ciudadFacade;

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public CiudadFacade getCiudadFacade() {
        return ciudadFacade;
    }
    public CiudadControlador() {
    }
    
    @PostConstruct
    public void init(){
        ciudad = new Ciudad();
    }

    @Override
    public Ciudad getObjectoEntidad(Integer i) {
        return this.getCiudadFacade().find(i);
    }
   
    public List<Ciudad> listaCiudades(){
        return getCiudadFacade().findAll();
    }
}