/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores.seguridad;

import com.sistemampc.modelos.Usuario;
import com.sistemampc.modulos.utilidades.FacesUtils;
import com.sistemampc.usuarios.bussines.UsuarioLoginSesionBean;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author APRENDIZ
 */
@Named
@RequestScoped
public class LoginControlador implements Serializable {

    private static final long serialVersionUID = 263321345L;

    private String numeroCedula;
    private String clave;

    @EJB
    private UsuarioLoginSesionBean usuarioLoginSesionBean;

    public String getNumeroCedula() {
        return numeroCedula;
    }

    public void setNumeroCedula(String numeroCedula) {
        this.numeroCedula = numeroCedula;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public LoginControlador() {
    }

    @PostConstruct
    public void init() {
        numeroCedula = "";
        clave = "";
    }

    public String iniciarSesion() {
        try {
            Object objeto = usuarioLoginSesionBean.autenticarUsuario(numeroCedula, clave);
            if (objeto instanceof Integer) {
                switch ((Integer) objeto) {
                    case 1:
                        FacesUtils.showFacesMessage(null, "Errot", "ocurrio un error", 3);
                        break;
                    case 2:
                        FacesUtils.showFacesMessage(null, "Errot", "ocurrio un error", 3);
                        break;
                    case 3:
                        FacesUtils.showFacesMessage(null, "Errot", "ocurrio un error", 3);
                        break;
                }
            } else if (objeto instanceof Usuario) {
                FacesUtils.setObjectSession("usuario", (Usuario) objeto);
                return "/protegido/usuario/listausuarios.mpc?faces-redirect=true";
            }
        } catch (Exception e) {
        }
        return null;
    }

    public String cerrarSesion() {
        FacesUtils.removeObjectSession("usuario");
        return "/index.mpc?faces-redirect=true";
    }

}
