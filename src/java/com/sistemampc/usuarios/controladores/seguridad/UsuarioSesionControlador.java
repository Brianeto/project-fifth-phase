/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores.seguridad;

import com.sistemampc.modelos.Usuario;
import com.sistemampc.modulos.utilidades.FacesUtils;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;

/**
 *
 * @author SENA
 */
@Named
@SessionScoped
public class UsuarioSesionControlador implements Serializable {

    private Usuario usuarioSesion;

    public Usuario getUsuarioSesion() {
        return usuarioSesion;
    }
    
    public UsuarioSesionControlador() {
    }
    
    @PostConstruct
    public void init(){
        usuarioSesion = (Usuario) FacesUtils.getObjectSession("usuario");
    }
    
    public boolean idFoto(){
        return !usuarioSesion.getFotoPerfil().isEmpty();
    }
    
}