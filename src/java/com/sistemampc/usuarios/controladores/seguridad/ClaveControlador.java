/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores.seguridad;

import com.sistemampc.modelos.Usuario;
import com.sistemampc.modulos.bussines.mail.Correo;
import com.sistemampc.modulos.bussines.mail.CorreoHtml;
import com.sistemampc.modulos.utilidades.FacesUtils;
import com.sistemampc.usuarios.bussines.UsuarioLoginSesionBean;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.inject.Inject;

/**
 *
 * @author SENA
 */
@Named
@RequestScoped
public class ClaveControlador implements Serializable{

    private String claveActual;
    private String nuevaClave;
    private String confirmaClave;

    public String getClaveActual() {
        return claveActual;
    }

    public void setClaveActual(String claveActual) {
        this.claveActual = claveActual;
    }

    public String getNuevaClave() {
        return nuevaClave;
    }

    public void setNuevaClave(String nuevaClave) {
        this.nuevaClave = nuevaClave;
    }

    public String getConfirmaClave() {
        return confirmaClave;
    }

    public void setConfirmaClave(String confirmaClave) {
        this.confirmaClave = confirmaClave;
    }
    
    
    @EJB 
    private UsuarioLoginSesionBean usuarioLoginSesionBean;
    
    @Inject
    private UsuarioSesionControlador usuarioSesionControlador;

    public UsuarioSesionControlador getUsuarioSesionControlador() {
        return usuarioSesionControlador;
    }
    
    public ClaveControlador() {
    }
    
    @PostConstruct
    public void init(){
        claveActual = "";
        nuevaClave = null;
        confirmaClave = null;
    }
    
    public void actualizaClave(){
        switch(usuarioLoginSesionBean.clavesIguales(usuarioSesionControlador.getUsuarioSesion(), claveActual)){
            case 1:
                FacesUtils.showFacesMessage(null, "Error", "Ocurrio un error a intentar validar su contraseña", 3);
                break;
            case 2:
                FacesUtils.showFacesMessage(null, "Error", "la contraseña digitada no es valida.", 3);
                claveActual = "";
                break;
            case 3:
                Object object = usuarioLoginSesionBean.confirmarNuevaClave(usuarioSesionControlador.getUsuarioSesion(), nuevaClave, confirmaClave);
                if(object instanceof Integer){
                    switch((Integer) object){
                        case 1:
                            FacesUtils.showFacesMessage("nuevaclave", "Error", "ocurrio un error", 1);
                            break;
                        case 2:
                            FacesUtils.showFacesMessage("nuevaclave", "Error", "digite una nueva clave", 1);
                            break;
                        case 3:
                            FacesUtils.showFacesMessage("nuevaclave", "Error", "la nueva clave no puede contener espacios", 1);
                            break;
                        case 4:
                            FacesUtils.showFacesMessage("nuevaclave", "Error", "debe contener minimo 8 caracteres", 1);
                            break;
                        case 5:
                            FacesUtils.showFacesMessage("nuevaclave", "Error", "debe contener números", 1);
                            break;
                        case 6:
                            FacesUtils.showFacesMessage("nuevaclave", "Error", "no puede contener caracteres especiales", 1);
                            break;
                        case 7:
                            FacesUtils.showFacesMessage("nuevaclave", "Error", "las contraseña no coinciden ", 1);
                            break;
                    }
                nuevaClave = "";
                confirmaClave = "";
                }else if(object instanceof Usuario){
                    FacesUtils.showFacesMessage(null, "Éxito", "contraseña actualizada", 1);
                    FacesUtils.setObjectSession("usuario", (Usuario) object);
                    claveActual = "";
                    nuevaClave = "";
                    confirmaClave = "";
                }
                break;  
        }
    }
    
}
