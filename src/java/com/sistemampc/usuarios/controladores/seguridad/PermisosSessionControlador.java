/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores.seguridad;

import com.sistemampc.modelos.Permiso;
import com.sistemampc.modulos.utilidades.FacesUtils;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 *
 * @author Dell
 */
@Named(value = "permisosSessionControlador")
@SessionScoped
public class PermisosSessionControlador implements Serializable {

    private static final long serialVersionUID = 7L;
    @Inject
    private UsuarioSesionControlador usc;
    private List<Permiso> listaDePermisos;
    private int rol;

    public PermisosSessionControlador() {
    }

    public UsuarioSesionControlador getUsc() {
        return usc;
    }

    public List<Permiso> getListaDePermisos() {
        return listaDePermisos;
    }

    public void setListaDePermisos(List<Permiso> listaDePermisos) {
        this.listaDePermisos = listaDePermisos;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    @PostConstruct
    public void init() {
        rol = getUsc().getUsuarioSesion().getIdRol().getIdRol();
        listaDePermisos = getUsc().getUsuarioSesion().getIdRol().getPermisoList();
    }

    public List<Permiso> permisosMenu() {
        try {
            List<Permiso> permisoMenu = new ArrayList<>();
            for (Permiso p : listaDePermisos) {
                if (p.getTipo().equals("m")) {
                    permisoMenu.add(p);
                }

            }
            return permisoMenu;

        } catch (Exception e) {
            return null;
        }
    }

    public String obtenerUrl(Permiso p) {
        return p.getUrl();
    }

    public boolean tienePermiso(int i) {
        for (Permiso p : listaDePermisos) {
            if (p.getIdPermiso().equals(i)) {
                return true;
            }
        }
        return false;
    }

    public void noPermiso(int i) {
        try {
            if (!tienePermiso(i)) {
                FacesUtils.redirect("/protegido/usuario/nopermiso.mpc");
            }
        } catch (IOException e) {
        }
    }
}
