package com.sistemampc.usuarios.controladores;

import com.sistemampc.modelos.Usuario;
import com.sistemampc.modulos.utilidades.FacesUtils;
import javax.inject.Named;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author SENA
 */
@Named
@RequestScoped
public class PerfilUsuarioControlador implements Serializable {

    private Usuario usuarioPerfil;

    public Usuario getUsuarioPerfil() {
        return usuarioPerfil;
    }

    public PerfilUsuarioControlador() {
    }
    
    @PostConstruct
    public void init(){
        usuarioPerfil = (Usuario) FacesUtils.getObjectSession("perfil");
    }
    
}