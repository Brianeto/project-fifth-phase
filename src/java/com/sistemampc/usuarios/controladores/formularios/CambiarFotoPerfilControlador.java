/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores.formularios;

import com.sistemampc.modelos.Usuario;
import com.sistemampc.modulos.facades.UsuarioFacade;
import com.sistemampc.modulos.utilidades.FacesUtils;
import com.sistemampc.modulos.utilidades.UploadFIleUtils;
import com.sistemampc.usuarios.controladores.seguridad.UsuarioSesionControlador;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.Part;

/**
 *
 * @author Dell
 */
@Named(value = "cambiarFotoPerfilControlador")
@RequestScoped
public class CambiarFotoPerfilControlador {

    private Part file;
    @EJB
    private UsuarioFacade uf;
    @Inject
    private UsuarioSesionControlador usc;

    public CambiarFotoPerfilControlador() {
    }

    public UsuarioSesionControlador getUsc() {
        return usc;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    @PostConstruct
    public void init() {
    }

    public void actualizarFotoPerfil() {
        try {
            Usuario usuario = getUsc().getUsuarioSesion();
            usuario.setFotoPerfil(UploadFIleUtils.uploadFile(file, String.valueOf(usuario.getNumeroCedula())));
            uf.edit(usuario);
            FacesUtils.setObjectSession("usuario", usuario);
            FacesUtils.renderId("main");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
