/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores;

import com.sistemampc.modelos.Permiso;
import com.sistemampc.modulos.facades.PermisoFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Pablo Andres Ramirez
 */
@Named(value = "permisoControlador")
@RequestScoped
public class PermisoControlador implements Serializable, IControlador<Permiso> {
    
    private static final long serialVersionUID = 2L;
    
    private Permiso permiso;
    @EJB private PermisoFacade permisoFacade;

    public Permiso getPermiso() {
        return permiso;
    }

    public void setPermiso(Permiso permiso) {
        this.permiso = permiso;
    }

    public PermisoFacade getPermisoFacade() {
        return permisoFacade;
    }
    
    public PermisoControlador() {
    }
    
    @PostConstruct
    public void init(){
        permiso = new Permiso();
    }

    @Override
    public Permiso getObjectoEntidad(Integer i) {
        return this.getPermisoFacade().find(i);
    }
    
}
