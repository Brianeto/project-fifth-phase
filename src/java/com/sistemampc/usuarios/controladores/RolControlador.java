/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores;

import com.sistemampc.modelos.Rol;
import com.sistemampc.modulos.facades.RolFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Pablo Andres Ramirez
 */
@Named(value = "rolControlador")
@RequestScoped
public class RolControlador implements Serializable, IControlador<Rol> {
    
    private static final long serialVersionUID = 2L;
    
    private Rol rol;
    @EJB private RolFacade rolFacade;

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public RolFacade getRolFacade() {
        return rolFacade;
    }
    
    public RolControlador() {
    }
    
    @PostConstruct
    public void init(){
        rol = new Rol();
    }

    @Override
    public Rol getObjectoEntidad(Integer i) {
        return this.getRolFacade().find(i);
    }
  
    public List<Rol> listaRoles(){
        return getRolFacade().findAll();
    }
}
