/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores;

import com.sistemampc.modelos.CategoriaLicencia;
import com.sistemampc.modulos.facades.CategoriaLicenciaFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Pablo Andres Ramirez
 */
@Named(value = "categoriaLicenciaControlador")
@RequestScoped
public class CategoriaLicenciaControlador implements Serializable, IControlador<CategoriaLicencia> {
    
    private static final long serialVersionUID = 2L;
    
    private CategoriaLicencia categoriaLicencia;
    @EJB private CategoriaLicenciaFacade categoriaLicenciaFacade;

    public CategoriaLicencia getCategoriaLicencia() {
        return categoriaLicencia;
    }

    public void setCategoriaLicencia(CategoriaLicencia categoriaLicencia) {
        this.categoriaLicencia = categoriaLicencia;
    }

    public CategoriaLicenciaFacade getCategoriaLicenciaFacade() {
        return categoriaLicenciaFacade;
    }
    
    public CategoriaLicenciaControlador() {
    }
    
    @PostConstruct
    public void init(){
        categoriaLicencia = new CategoriaLicencia();
    }

    @Override
    public CategoriaLicencia getObjectoEntidad(Integer i) {
        return this.getCategoriaLicenciaFacade().find(i);
    }
    
    public List<CategoriaLicencia> listaCategoriasLicencias(){
        return getCategoriaLicenciaFacade().findAll();
    }
}
