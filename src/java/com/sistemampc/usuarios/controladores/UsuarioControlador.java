/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores;

import com.sistemampc.modelos.Conductor;
import com.sistemampc.modelos.TelefonoUsuario;
import com.sistemampc.modelos.Usuario;
import com.sistemampc.modulos.bussines.mail.Correo;
import com.sistemampc.modulos.bussines.mail.CorreoHtml;
import com.sistemampc.modulos.facades.ConductorFacade;
import com.sistemampc.modulos.facades.TelefonoUsuarioFacade;
import com.sistemampc.modulos.facades.UsuarioFacade;
import com.sistemampc.modulos.utilidades.FacesUtils;
import com.sistemampc.modulos.utilidades.IControlador;
import com.sistemampc.usuarios.controladores.utilidades.FechaUtils;
import com.sistemampc.usuarios.controladores.utilidades.GenerarPassword;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Pablo Andres Ramirez
 */
@Named(value = "usuarioControlador")
@ConversationScoped
public class UsuarioControlador implements Serializable, IControlador<Usuario> {

    private static final long serialVersionUID = 2L;

    @Inject
    private Conversation conversation;
    private boolean transientVal = true;

    private Usuario usuario;
    private TelefonoUsuario telefonoUsuario;
    private Conductor conductor;
    @EJB
    private UsuarioFacade usuarioFacade;
    @EJB
    private TelefonoUsuarioFacade telefonoUsuarioFacade;
    @EJB
    private ConductorFacade conductorFacade;

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public boolean isTransientVal() {
        return transientVal;
    }

    public void setTransientVal(boolean transientVal) {
        this.transientVal = transientVal;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TelefonoUsuario getTelefonoUsuario() {
        return telefonoUsuario;
    }

    public void setTelefonoUsuario(TelefonoUsuario telefonoUsuario) {
        this.telefonoUsuario = telefonoUsuario;
    }

    public Conductor getConductor() {
        return conductor;
    }

    public void setConductor(Conductor conductor) {
        this.conductor = conductor;
    }

    public UsuarioFacade getUsuarioFacade() {
        return usuarioFacade;
    }

    public TelefonoUsuarioFacade getTelefonoUsuarioFacade() {
        return telefonoUsuarioFacade;
    }

    public ConductorFacade getConductorFacade() {
        return conductorFacade;
    }

    public UsuarioControlador() {
    }

    @PostConstruct
    public void init() {
        usuario = new Usuario();
        telefonoUsuario = new TelefonoUsuario();
        conductor = new Conductor();
    }

    @Override
    public Usuario getObjectoEntidad(Integer i) {
        return getUsuarioFacade().find(i);
    }

    public List<Usuario> listaUsuario(){
        return usuarioFacade.findAll();
    }
    // Medotodos crud
    public String registrarUsuario() {
        boolean registrar = true;
        try {
            if (getUsuarioFacade().usuarioCedula(getUsuario().getNumeroCedula())) {
                FacesUtils.showFacesMessage("global", "Advertencia", "El número de cédula ya existe.", 4);
                registrar = false;
            }
            if (getUsuarioFacade().usuarioCorreo(getUsuario().getCorreoElectronico())) {
                FacesUtils.showFacesMessage("global", "Advertencia", "El correo electrónico ya existe.", 4);
                registrar = false;
            }
            if (registrar) {
                getUsuario().setIdioma("es");
                getUsuario().setClave(GenerarPassword.generarClave(4));
                String clave = getUsuario().getClave();
                
                getUsuarioFacade().registrarUsuario(getUsuario());
                setUsuario(getUsuarioFacade().buscarUsuarioNumeroCedula(getUsuario().getNumeroCedula()));
                getTelefonoUsuario().setIdUsuarioTelefono(getUsuario());
                getTelefonoUsuarioFacade().create(getTelefonoUsuario());
                FacesUtils.showFacesMessage("Éxito", "Usuario registrado con éxito", 2);
                System.out.println(clave);
                Correo correo = new CorreoHtml();
                correo.enviarCorreo("clave", "su clave es " + clave, getUsuario().getCorreoElectronico());
                FacesUtils.renderId("global");
                if (getUsuario().getIdRol().getIdRol().equals(6)) {
                    conversation.begin();
                    transientVal = conversation.isTransient();
                    return "/conductor.mpc?faces-redirect=true";
                }
                
            }
        } catch (Exception e) {
        }
        setUsuario(null);
        setTelefonoUsuario(null);
        return null;
    }

    public String registrarConductor() {
        getConductor().setIdUsuarioConductor(usuario.getIdUsuario());
        getConductor().setUsuario(usuario);
        getConductorFacade().create(getConductor());
        conversation.end();
        transientVal = conversation.isTransient();
        return "/index.mpc?faces-redirect=true";
    }
    
    public void transacionRegistro() throws IOException{
        if(isTransientVal()){
            FacesUtils.redirect("/index.mpc");
        }
    }
    
    public String calcularEdad(Usuario usuario){
        try {
            return FechaUtils.carcularEdad(usuario.getFechaNacimiento()) + " años";
        } catch (Exception e) {
            return "0 años";
        }
    }
}