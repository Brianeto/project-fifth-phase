/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores.convertidores;

import com.sistemampc.modelos.Permiso;
import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Pablo Andres Ramirez
 */
@FacesConverter(forClass = Permiso.class)
public class PermisoControlador extends AbstractConvertidor{

    public PermisoControlador() {
        this.nombreControlador = "permisoControlador";
    }
    
}
