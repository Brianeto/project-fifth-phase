/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores.convertidores;

import com.sistemampc.modelos.Rol;
import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Pablo Andres Ramirez
 */
@FacesConverter(forClass = Rol.class)
public class RolConvertidor extends AbstractConvertidor{

    public RolConvertidor() {
        this.nombreControlador = "rolControlador";
    }
    
}
