/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores.convertidores;

import com.sistemampc.modelos.Ciudad;
import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Pablo Andres Ramirez
 */
@FacesConverter(forClass = Ciudad.class)
public class CiudadConvertidor extends AbstractConvertidor{

    public CiudadConvertidor() {
        this.nombreControlador = "ciudadControlador";
    }
    
}
