/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.usuarios.controladores.convertidores;

import com.sistemampc.modelos.CategoriaLicencia;
import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Pablo Andres Ramirez
 */
@FacesConverter(forClass = CategoriaLicencia.class)
public class CategoriaLicenciaConvertidor extends AbstractConvertidor{

    public CategoriaLicenciaConvertidor() {
        this.nombreControlador = "categoriaLicenciaControlador";
    }
    
}
