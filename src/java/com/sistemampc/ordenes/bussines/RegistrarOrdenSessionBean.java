/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.bussines;

import com.sistemampc.modelos.Orden;
import com.sistemampc.modelos.Usuario;
import com.sistemampc.modelos.Vehiculo;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Dell
 */
@Stateless
public class RegistrarOrdenSessionBean {

    @PersistenceContext(unitName = "sistemampcPU")
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }

    public RegistrarOrdenSessionBean() {
    }

    public Object registrarOrden(Orden orden, Vehiculo vehiculo, Usuario usuario) {
        try {
            /* Estados 1:Pendiente mantenimiento, 2: Pendiente revisión, 
            3:en mantenimiento, 4: En revisión, 5: Disponible 6: inactivo*/
            if (vehiculo.getEstadoVehiculo() == 5) {
                orden.setIdUsuario(usuario);
                orden.setIdVehiculo(vehiculo);
                orden.setFechaEmision(new Date());
                getEntityManager().persist(orden);
                System.out.println(orden.getTipoOrden());
                if (orden.getTipoOrden() == 2) {
                    orden.setEstadoOrden((short) 2);
                    vehiculo.setEstadoVehiculo((short) 2);
                }
                if (orden.getTipoOrden() == 1) {
                    orden.setEstadoOrden((short) 1);
                    vehiculo.setEstadoVehiculo((short) 1);
                }
                getEntityManager().merge(vehiculo);
                return vehiculo;
            } else {
                return 2;
            }
        } catch (Exception e) {
            return 1;
        }
    }
}
