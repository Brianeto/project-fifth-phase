/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores;

import com.sistemampc.modelos.Orden;
import com.sistemampc.modulos.facades.OrdenFacade;
import com.sistemampc.modulos.utilidades.FacesUtils;
import com.sistemampc.modulos.utilidades.IControlador;
import com.sistemampc.usuarios.controladores.utilidades.FechaUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Hector
 */
@Named(value = "ordenControlador")
@RequestScoped
public class OrdenControlador implements Serializable, IControlador<Orden> {

    private static final long serialVersionUID = 1L;

    private Orden orden;
    @EJB
    private OrdenFacade of;

    public OrdenControlador() {
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    @PostConstruct
    public void init() {
        orden = new Orden();
    }

    @Override
    public Orden getObjectoEntidad(Integer i) {
        return of.find(i);
    }

    public List<Orden> listaOrdenes() {
        return of.findAll();
    }
    
    public List<Orden> listarOrdenesPorTipoPorEstado(short i){
        List<Orden> lista = new ArrayList<>();
        for(Orden o: listaOrdenes()){
            if (o.getEstadoOrden()==i) {
                lista.add(o);
            }
        }
        return lista;
    }
    public String tipoOrden(short i) {
        if (i == 1) {
            return "Mantenimiento";
        } else if (i == 2) {
            return "Revisión";
        }
        return null;
    }

    /* Estados 1:Pendiente mantenimiento, 2: Pendiente revisión, 
            3:en mantenimiento, 4: En revisión, 5: Disponible 6: inactivo*/
    public String estadoOrden(short i) {
        switch (i) {
            case 1:
                return "Pendiente";
            case 2:
                return "Pendiente";
            case 3:
                return "En mantenimiento";
            case 4:
                return "En revisión";
            case 5:
                return "Disponible";
            case 6:
                return "Inactivo";
            default:
                return null;
        }
    }
    public String fechaEmisionCaracteres(Date fecha){
       return FechaUtils.caracteresFecha(fecha);
    }
    
    public String verFichaOrden(Orden o) {
        FacesUtils.setObjectSession("orden", o);
        return "/protegido/orden/ficha/fichaorden.mpc?faces-redirect=true";
    }
}
