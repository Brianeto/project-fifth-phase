/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores;

import com.sistemampc.modelos.Mantenimiento;
import com.sistemampc.modelos.TipoFalla;
import com.sistemampc.modulos.facades.MantenimientoFacade;
import com.sistemampc.modulos.facades.TipoFallaFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Dell
 */
@Named(value = "mantenimientoControlador")
@RequestScoped
public class MantenimientoControlador implements Serializable, IControlador<Mantenimiento> {

    private static final long serialVersionUID = 3L;

    private Mantenimiento mantenimiento;
    private TipoFalla tipoFalla;
    @EJB
    private MantenimientoFacade mf;
    @EJB
    private TipoFallaFacade tff;

    public MantenimientoControlador() {
    }

    public Mantenimiento getMantenimiento() {
        return mantenimiento;
    }

    public void setMantenimiento(Mantenimiento mantenimiento) {
        this.mantenimiento = mantenimiento;
    }

    @PostConstruct
    public void init() {
        mantenimiento = new Mantenimiento();
        tipoFalla = new TipoFalla();
    }

    @Override
    public Mantenimiento getObjectoEntidad(Integer i) {
        return mf.find(i);
    }

    public List<Mantenimiento> listaUsuario() {
        return mf.findAll();
    }

}
