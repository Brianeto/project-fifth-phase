package com.sistemampc.ordenes.controladores.graficas;

import com.sistemampc.modelos.Orden;
import com.sistemampc.ordenes.controladores.OrdenControlador;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.inject.Inject;

@Named
@RequestScoped
public class GraficaMotivoListaOrdenesControlador implements Serializable {

    @Inject
    private OrdenControlador ordenControlador;

    public OrdenControlador getOrdenControlador() {
        return ordenControlador;
    }

    public GraficaMotivoListaOrdenesControlador() {
    }

    @PostConstruct
    public void init() {
    }

    public int mostrarMotivo(int i) {
        int numero = 0;
        for (Orden o : getOrdenControlador().listaOrdenes()) {
            if (o.getIdMotivo().getIdMotivo() == i) {
                numero++;
            }
        }
        return numero;
    }

}
