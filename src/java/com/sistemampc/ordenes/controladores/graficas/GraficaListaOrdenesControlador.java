package com.sistemampc.ordenes.controladores.graficas;

import com.sistemampc.modelos.Orden;
import com.sistemampc.ordenes.controladores.OrdenControlador;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.inject.Inject;

@Named
@RequestScoped
public class GraficaListaOrdenesControlador implements Serializable{
    
    @Inject
    private OrdenControlador ordenControlador;
    
    private int pendienteMantenimiento;
    private int pendienteRevision;
    

    public int getPendienteMantenimiento() {
        return pendienteMantenimiento;
    }

    public void setPendienteMantenimiento(int pendienteMantenimiento) {
        this.pendienteMantenimiento = pendienteMantenimiento;
    }

    public int getPendienteRevision() {
        return pendienteRevision;
    }

    public void setPendienteRevision(int pendienteRevision) {
        this.pendienteRevision = pendienteRevision;
    }
    
    public OrdenControlador getOrdenControlador() {
        return ordenControlador;
    }
    
    public GraficaListaOrdenesControlador() {
    }
    
    @PostConstruct
    public void init(){
        pendienteRevision = this.asignarPendienteMantenimeinto((short)2);
        pendienteMantenimiento = this.asignarPendienteMantenimeinto((short)1);
    }
    
    public int asignarPendienteMantenimeinto(short i){
        int numero = 0;
        for(Orden o: getOrdenControlador().listaOrdenes()){
            if(o.getEstadoOrden()==i){
                numero++;
            }
        }
        return numero;
    }
    
}
