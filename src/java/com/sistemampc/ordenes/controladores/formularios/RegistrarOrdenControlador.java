/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores.formularios;

import com.sistemampc.modelos.Orden;
import com.sistemampc.modelos.Usuario;
import com.sistemampc.modelos.Vehiculo;
import com.sistemampc.modulos.utilidades.FacesUtils;
import com.sistemampc.ordenes.bussines.RegistrarOrdenSessionBean;
import com.sistemampc.usuarios.controladores.seguridad.UsuarioSesionControlador;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Dell
 */
@Named(value = "registrarOrdenControlador")
@ConversationScoped
public class RegistrarOrdenControlador implements Serializable {

    private Orden orden;
    private short tipoOrden;
    private Vehiculo vehiculo;
    @Inject
    private Conversation conver;
    @Inject
    private UsuarioSesionControlador usc;
    @EJB
    private RegistrarOrdenSessionBean rosb;

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public UsuarioSesionControlador getUsc() {
        return usc;
    }

    public short getTipoOrden() {
        return tipoOrden;
    }

    public void setTipoOrden(short tipoOrden) {
        this.tipoOrden = tipoOrden;
    }
    
    public RegistrarOrdenControlador() {
    }

    @PostConstruct
    public void init() {
        orden = new Orden();
        vehiculo = new Vehiculo();
    }

    public String registrarOrdenn() {
        orden.setIdUsuario(usc.getUsuarioSesion());
        orden.setIdVehiculo((Vehiculo) FacesUtils.getObjectSession("vehiculo"));
        orden.setFechaEmision(new Date());
        orden.setEstadoOrden((short) 1);
        return null;
    }

    public String registrarOrden() {
        try {
            Object obj = rosb.registrarOrden(orden, (Vehiculo) FacesUtils.getObjectSession("vehiculo"), (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario"));
            if (obj instanceof Integer) {
                switch ((Integer) obj) {
                    case 1:
                        FacesUtils.showFacesMessage(null, "Error", "Error", 1);
                        break;
                    case 2:
                        FacesUtils.showFacesMessage(null, "Error Ya tiene asiganada orden", "Error", 1);
                        break;
                }
            } else if (obj instanceof Vehiculo) {
                FacesUtils.setObjectSession("vehiculo", obj);
                return "/protegido/vehiculo/ficha/fichatecnica.mpc?faces-redirect=true";
            }
        } catch (Exception e) {

        }
        return null;
    }

}
