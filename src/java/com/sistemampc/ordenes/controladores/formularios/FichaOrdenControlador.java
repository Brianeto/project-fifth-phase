/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores.formularios;

import com.sistemampc.modelos.Orden;
import com.sistemampc.modulos.utilidades.FacesUtils;
import javax.inject.Named;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author SENA
 */
@Named
@RequestScoped
public class FichaOrdenControlador implements Serializable {

    private Orden orderFicha;

    public Orden getOrderFicha() {
        return orderFicha;
    }
    
    public FichaOrdenControlador() {
    }

    @PostConstruct
    public void init() {
        orderFicha = (Orden) FacesUtils.getObjectSession("orden");
    }
    
    public String verUsuario(Object u){
        FacesUtils.setObjectSession("perfil", u);
        return "/protegido/usuario/perfil/perfilusuario.mpc?faces-redirect=true";
    }
}
