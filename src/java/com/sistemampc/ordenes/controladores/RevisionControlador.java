/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores;

import com.sistemampc.modelos.Revision;
import com.sistemampc.modulos.facades.RevisionFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Dell
 */
@Named(value = "revisionControlador")
@RequestScoped
public class RevisionControlador implements Serializable, IControlador<Revision> {

    private static final long serialVersionUID = 2L;

    private Revision revision;
    @EJB
    private RevisionFacade rf;

    public RevisionControlador() {
    }

    public Revision getRevision() {
        return revision;
    }

    public void setRevision(Revision revision) {
        this.revision = revision;
    }

    @PostConstruct
    public void init() {
        revision = new Revision();
    }

    @Override
    public Revision getObjectoEntidad(Integer i) {
        return rf.find(i);
    }

    public List<Revision> listarRevisiones() {
        return rf.findAll();
    }

}
