/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores.convertidores;

import com.sistemampc.modelos.TipoFalla;
import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Dell
 */
@FacesConverter(forClass = TipoFalla.class)
public class TipoFallaConvertidor extends AbstractConvertidor {

    public TipoFallaConvertidor() {
        this.nombreControlador = "tipoFallaControlador";
    }
}
