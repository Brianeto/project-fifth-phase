/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores.convertidores;

import com.sistemampc.modelos.MotivoOrden;
import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Dell
 */
@FacesConverter(forClass = MotivoOrden.class)
public class MotivosOrdenConvertidor extends AbstractConvertidor {

    public MotivosOrdenConvertidor() {
        this.nombreControlador = "motivosOrdenControlador";
    }

}
