/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores.convertidores;

import com.sistemampc.modelos.Mantenimiento;
import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Dell
 */
@FacesConverter(forClass = Mantenimiento.class)
public class MantenimientoConvertidor extends AbstractConvertidor {

    public MantenimientoConvertidor() {
        this.nombreControlador = "mantenimientoControlador";
    }

}
