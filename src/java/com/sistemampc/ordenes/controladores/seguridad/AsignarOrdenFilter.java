/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores.seguridad;

import com.sistemampc.modelos.Vehiculo;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dell
 */
@WebFilter(filterName = "AsignarOrdenFilter", urlPatterns = {"/protegido/orden/vehiculo/*"})
public class AsignarOrdenFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        try {
            if (req.getSession().getAttribute("vehiculo") == null) {
                res.sendRedirect(req.getContextPath() + "/protegido/vehiculo/listavehiculos.mpc");
            } else {
                Vehiculo v = (Vehiculo) req.getSession().getAttribute("vehiculo");
                if (v.getEstadoVehiculo()!=5) {
                    res.sendRedirect(req.getContextPath() + "/protegido/vehiculo/ficha/fichatecnica.mpc");
                }
            }
        } catch (Exception e) {
            res.sendRedirect(req.getContextPath() + "/protegido/vehiculo/listavehiculos.mpc");
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

}
