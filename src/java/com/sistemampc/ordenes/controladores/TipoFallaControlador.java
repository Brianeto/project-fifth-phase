/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores;

import com.sistemampc.modelos.TipoFalla;
import com.sistemampc.modulos.facades.TipoFallaFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Dell
 */
@Named(value = "tipoFallaControlador")
@RequestScoped
public class TipoFallaControlador implements Serializable, IControlador<TipoFalla> {

    private TipoFalla tipoFalla;
    @EJB
    private TipoFallaFacade tff;

    public TipoFallaControlador() {
    }

    public TipoFalla getTipoFalla() {
        return tipoFalla;
    }

    public void setTipoFalla(TipoFalla tipoFalla) {
        this.tipoFalla = tipoFalla;
    }

    @PostConstruct
    public void init() {
        tipoFalla = new TipoFalla();
    }

    @Override
    public TipoFalla getObjectoEntidad(Integer i) {
        return tff.find(i);
    }

    public List<TipoFalla> listaTipoFalla() {
        return tff.findAll();
    }

}
