/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.ordenes.controladores;

import com.sistemampc.modelos.MotivoOrden;
import com.sistemampc.modulos.facades.MotivoOrdenFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Dell
 */
@Named(value = "motivosOrdenControlador")
@RequestScoped
public class MotivosOrdenControlador implements Serializable, IControlador<MotivoOrden> {

    private MotivoOrden motivoOrden;
    @EJB
    private MotivoOrdenFacade mof;

    public MotivosOrdenControlador() {
    }

    public MotivoOrden getMotivoOrden() {
        return motivoOrden;
    }

    public void setMotivoOrden(MotivoOrden motivoOrden) {
        this.motivoOrden = motivoOrden;
    }

    @PostConstruct
    public void init() {
        motivoOrden = new MotivoOrden();
    }

    @Override
    public MotivoOrden getObjectoEntidad(Integer i) {
        return mof.find(i);
    }

    public List<MotivoOrden> listarMotivoOrden() {
        return mof.findAll();
    }

}
