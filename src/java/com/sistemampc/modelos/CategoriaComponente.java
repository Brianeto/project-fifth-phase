/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.modelos;

import com.sistemampc.modulos.utilidades.IEntity;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author APRENDIZ
 */
@Entity
@Table(name = "categorias_componentes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriaComponente.findAll", query = "SELECT c FROM CategoriaComponente c"),
    @NamedQuery(name = "CategoriaComponente.findByIdCategoriaComponente", query = "SELECT c FROM CategoriaComponente c WHERE c.idCategoriaComponente = :idCategoriaComponente"),
    @NamedQuery(name = "CategoriaComponente.findByNombreCategoriaComponente", query = "SELECT c FROM CategoriaComponente c WHERE c.nombreCategoriaComponente = :nombreCategoriaComponente")})
public class CategoriaComponente implements Serializable, IEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_categoria_componente")
    private Integer idCategoriaComponente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "nombre_categoria_componente")
    private String nombreCategoriaComponente;
    @Lob
    @Size(max = 65535)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCategoriaComponenteComponente", fetch = FetchType.LAZY)
    private List<Componente> componenteList;

    public CategoriaComponente() {
    }

    public CategoriaComponente(Integer idCategoriaComponente) {
        this.idCategoriaComponente = idCategoriaComponente;
    }

    public CategoriaComponente(Integer idCategoriaComponente, String nombreCategoriaComponente) {
        this.idCategoriaComponente = idCategoriaComponente;
        this.nombreCategoriaComponente = nombreCategoriaComponente;
    }

    public Integer getIdCategoriaComponente() {
        return idCategoriaComponente;
    }

    public void setIdCategoriaComponente(Integer idCategoriaComponente) {
        this.idCategoriaComponente = idCategoriaComponente;
    }

    public String getNombreCategoriaComponente() {
        return nombreCategoriaComponente;
    }

    public void setNombreCategoriaComponente(String nombreCategoriaComponente) {
        this.nombreCategoriaComponente = nombreCategoriaComponente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Componente> getComponenteList() {
        return componenteList;
    }

    public void setComponenteList(List<Componente> componenteList) {
        this.componenteList = componenteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoriaComponente != null ? idCategoriaComponente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaComponente)) {
            return false;
        }
        CategoriaComponente other = (CategoriaComponente) object;
        if ((this.idCategoriaComponente == null && other.idCategoriaComponente != null) || (this.idCategoriaComponente != null && !this.idCategoriaComponente.equals(other.idCategoriaComponente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sistemampc.modelos.CategoriaComponente[ idCategoriaComponente=" + idCategoriaComponente + " ]";
    }
    
    @Override
    public String getLlavePrimaria() {
        return idCategoriaComponente.toString();
    }
}
