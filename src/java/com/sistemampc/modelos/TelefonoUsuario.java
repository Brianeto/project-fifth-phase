/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.modelos;

import com.sistemampc.modulos.utilidades.IEntity;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author APRENDIZ
 */
@Entity
@Table(name = "telefonos_usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TelefonoUsuario.findAll", query = "SELECT t FROM TelefonoUsuario t"),
    @NamedQuery(name = "TelefonoUsuario.findByIdTelefono", query = "SELECT t FROM TelefonoUsuario t WHERE t.idTelefono = :idTelefono"),
    @NamedQuery(name = "TelefonoUsuario.findByTelefono", query = "SELECT t FROM TelefonoUsuario t WHERE t.telefono = :telefono")})
public class TelefonoUsuario implements Serializable, IEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_telefono")
    private Integer idTelefono;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "telefono")
    private String telefono;
    @JoinColumn(name = "id_usuario_telefono", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usuario idUsuarioTelefono;

    public TelefonoUsuario() {
    }

    public TelefonoUsuario(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public TelefonoUsuario(Integer idTelefono, String telefono) {
        this.idTelefono = idTelefono;
        this.telefono = telefono;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Usuario getIdUsuarioTelefono() {
        return idUsuarioTelefono;
    }

    public void setIdUsuarioTelefono(Usuario idUsuarioTelefono) {
        this.idUsuarioTelefono = idUsuarioTelefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTelefono != null ? idTelefono.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TelefonoUsuario)) {
            return false;
        }
        TelefonoUsuario other = (TelefonoUsuario) object;
        if ((this.idTelefono == null && other.idTelefono != null) || (this.idTelefono != null && !this.idTelefono.equals(other.idTelefono))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sistemampc.modelos.TelefonoUsuario[ idTelefono=" + idTelefono + " ]";
    }
    
    @Override
    public String getLlavePrimaria() {
        return idTelefono.toString();
    }
}
