/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.modelos;

import com.sistemampc.modulos.utilidades.IEntity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author APRENDIZ
 */
@Entity
@Table(name = "documentos_vehiculos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoVehiculo.findAll", query = "SELECT d FROM DocumentoVehiculo d"),
    @NamedQuery(name = "DocumentoVehiculo.findByIdDocumentoVehiculo", query = "SELECT d FROM DocumentoVehiculo d WHERE d.idDocumentoVehiculo = :idDocumentoVehiculo"),
    @NamedQuery(name = "DocumentoVehiculo.findByIdEstadoDocumento", query = "SELECT d FROM DocumentoVehiculo d WHERE d.idEstadoDocumento = :idEstadoDocumento"),
    @NamedQuery(name = "DocumentoVehiculo.findByNombreDocumento", query = "SELECT d FROM DocumentoVehiculo d WHERE d.nombreDocumento = :nombreDocumento"),
    @NamedQuery(name = "DocumentoVehiculo.findByFechaAdquisicion", query = "SELECT d FROM DocumentoVehiculo d WHERE d.fechaAdquisicion = :fechaAdquisicion"),
    @NamedQuery(name = "DocumentoVehiculo.findByFechaVencimiento", query = "SELECT d FROM DocumentoVehiculo d WHERE d.fechaVencimiento = :fechaVencimiento")})
public class DocumentoVehiculo implements Serializable, IEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_documento_vehiculo")
    private Integer idDocumentoVehiculo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado_documento")
    private boolean idEstadoDocumento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre_documento")
    private String nombreDocumento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_adquisicion")
    @Temporal(TemporalType.DATE)
    private Date fechaAdquisicion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    @Lob
    @Size(max = 65535)
    @Column(name = "documento_adjunto")
    private String documentoAdjunto;
    @JoinColumn(name = "id_vehiculo", referencedColumnName = "id_vehiculo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Vehiculo idVehiculo;
    @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id_tipo_documento")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoDocumento idTipoDocumento;

    public DocumentoVehiculo() {
    }

    public DocumentoVehiculo(Integer idDocumentoVehiculo) {
        this.idDocumentoVehiculo = idDocumentoVehiculo;
    }

    public DocumentoVehiculo(Integer idDocumentoVehiculo, boolean idEstadoDocumento, String nombreDocumento, Date fechaAdquisicion, Date fechaVencimiento) {
        this.idDocumentoVehiculo = idDocumentoVehiculo;
        this.idEstadoDocumento = idEstadoDocumento;
        this.nombreDocumento = nombreDocumento;
        this.fechaAdquisicion = fechaAdquisicion;
        this.fechaVencimiento = fechaVencimiento;
    }

    public Integer getIdDocumentoVehiculo() {
        return idDocumentoVehiculo;
    }

    public void setIdDocumentoVehiculo(Integer idDocumentoVehiculo) {
        this.idDocumentoVehiculo = idDocumentoVehiculo;
    }

    public boolean getIdEstadoDocumento() {
        return idEstadoDocumento;
    }

    public void setIdEstadoDocumento(boolean idEstadoDocumento) {
        this.idEstadoDocumento = idEstadoDocumento;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public Date getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(Date fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getDocumentoAdjunto() {
        return documentoAdjunto;
    }

    public void setDocumentoAdjunto(String documentoAdjunto) {
        this.documentoAdjunto = documentoAdjunto;
    }

    public Vehiculo getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Vehiculo idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public TipoDocumento getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(TipoDocumento idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDocumentoVehiculo != null ? idDocumentoVehiculo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoVehiculo)) {
            return false;
        }
        DocumentoVehiculo other = (DocumentoVehiculo) object;
        if ((this.idDocumentoVehiculo == null && other.idDocumentoVehiculo != null) || (this.idDocumentoVehiculo != null && !this.idDocumentoVehiculo.equals(other.idDocumentoVehiculo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sistemampc.modelos.DocumentoVehiculo[ idDocumentoVehiculo=" + idDocumentoVehiculo + " ]";
    }
  
    @Override
    public String getLlavePrimaria() {
        return idDocumentoVehiculo.toString();
    }
}
