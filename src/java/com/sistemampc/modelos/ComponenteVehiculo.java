/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.modelos;

import com.sistemampc.modulos.utilidades.IEntity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SENA
 */
@Entity
@Table(name = "componentes_vehiculos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComponenteVehiculo.findAll", query = "SELECT c FROM ComponenteVehiculo c")
    , @NamedQuery(name = "ComponenteVehiculo.findByIdComponenteVehiculo", query = "SELECT c FROM ComponenteVehiculo c WHERE c.idComponenteVehiculo = :idComponenteVehiculo")
    , @NamedQuery(name = "ComponenteVehiculo.findByEstadoComponente", query = "SELECT c FROM ComponenteVehiculo c WHERE c.estadoComponente = :estadoComponente")
    , @NamedQuery(name = "ComponenteVehiculo.findByIdVehiculo", query = "SELECT c FROM ComponenteVehiculo c WHERE c.idVehiculo = :idvehiculo")
    , @NamedQuery(name = "ComponenteVehiculo.findByUltimaFechaCambio", query = "SELECT c FROM ComponenteVehiculo c WHERE c.ultimaFechaCambio = :ultimaFechaCambio")})
public class ComponenteVehiculo implements Serializable, IEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_componente_vehiculo")
    private Integer idComponenteVehiculo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado_componente")
    private boolean estadoComponente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ultima_fecha_cambio")
    @Temporal(TemporalType.DATE)
    private Date ultimaFechaCambio;
    @JoinColumn(name = "id_vehiculo", referencedColumnName = "id_vehiculo")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Vehiculo idVehiculo;
    @JoinColumn(name = "id_componente", referencedColumnName = "id_componente")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Componente idComponente;

    public ComponenteVehiculo() {
    }

    public ComponenteVehiculo(Integer idComponenteVehiculo) {
        this.idComponenteVehiculo = idComponenteVehiculo;
    }

    public ComponenteVehiculo(Integer idComponenteVehiculo, boolean estadoComponente, Date ultimaFechaCambio) {
        this.idComponenteVehiculo = idComponenteVehiculo;
        this.estadoComponente = estadoComponente;
        this.ultimaFechaCambio = ultimaFechaCambio;
    }

    public Integer getIdComponenteVehiculo() {
        return idComponenteVehiculo;
    }

    public void setIdComponenteVehiculo(Integer idComponenteVehiculo) {
        this.idComponenteVehiculo = idComponenteVehiculo;
    }

    public boolean getEstadoComponente() {
        return estadoComponente;
    }

    public void setEstadoComponente(boolean estadoComponente) {
        this.estadoComponente = estadoComponente;
    }

    public Date getUltimaFechaCambio() {
        return ultimaFechaCambio;
    }

    public void setUltimaFechaCambio(Date ultimaFechaCambio) {
        this.ultimaFechaCambio = ultimaFechaCambio;
    }

    public Vehiculo getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Vehiculo idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public Componente getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(Componente idComponente) {
        this.idComponente = idComponente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComponenteVehiculo != null ? idComponenteVehiculo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComponenteVehiculo)) {
            return false;
        }
        ComponenteVehiculo other = (ComponenteVehiculo) object;
        if ((this.idComponenteVehiculo == null && other.idComponenteVehiculo != null) || (this.idComponenteVehiculo != null && !this.idComponenteVehiculo.equals(other.idComponenteVehiculo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sistemampc.modelos.ComponenteVehiculo[ idComponenteVehiculo=" + idComponenteVehiculo + " ]";
    }
    
    @Override
    public String getLlavePrimaria() {
        return idComponenteVehiculo.toString();
    }
}
