/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.componentes.controladores;

import com.sistemampc.modulos.facades.CategoriaComponenteFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import com.sistemampc.modelos.CategoriaComponente;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Brianeto
 */
@Named(value = "categoriaComponenteControlador")
@RequestScoped
public class CategoriaComponenteControlador implements Serializable, IControlador<CategoriaComponente> {
    
    private static final long serialVersionUID = 2L;
    
    private CategoriaComponente categoriaComponente;
    @EJB private CategoriaComponenteFacade categoriaComponenteFacade;

    public CategoriaComponente getCategoriaComponente() {
        return categoriaComponente;
    }

    public void setCategoriaComponente(CategoriaComponente categoriaComponente) {
        this.categoriaComponente = categoriaComponente;
    }

    public CategoriaComponenteFacade getCategoriaComponenteFacade() {
        return categoriaComponenteFacade;
    }
    
    public CategoriaComponenteControlador() {
    }
    
    @PostConstruct
    public void init(){
        categoriaComponente = new CategoriaComponente();
    }

    @Override
    public CategoriaComponente getObjectoEntidad(Integer i) {
        return this.getCategoriaComponenteFacade().find(i);
    }
    
    public List<CategoriaComponente> listaCategoriasComponentes(){
        return getCategoriaComponenteFacade().findAll();
    }
}
