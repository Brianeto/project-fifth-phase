/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.componentes.controladores;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Johaneto
 */
@ApplicationScoped
public class AsignarComponenteControlador {
    private String nombreComponente;
    private String vidaUtil;
    private String aniosMeses;
    private String idCategoriaComponenteComponente;
    
    private ArrayList<String> test = new ArrayList<String>();

public AsignarComponenteControlador() {
}

public AsignarComponenteControlador(String nombreComponente) {
    this.nombreComponente = nombreComponente;
}


public String addtoList(){
    test.add(this.nombreComponente);
    return "./fichatecnica.xhtml";
} 

    public String getNombreComponente() {
        return nombreComponente;
    }

    public void setNombreComponente(String nombreComponente) {
        this.nombreComponente = nombreComponente;
    }

    public String getVidaUtil() {
        return vidaUtil;
    }

    public void setVidaUtil(String vidaUtil) {
        this.vidaUtil = vidaUtil;
    }

    public String getAniosMeses() {
        return aniosMeses;
    }

    public void setAniosMeses(String aniosMeses) {
        this.aniosMeses = aniosMeses;
    }

    public String getIdCategoriaComponenteComponente() {
        return idCategoriaComponenteComponente;
    }

    public void setIdCategoriaComponenteComponente(String idCategoriaComponenteComponente) {
        this.idCategoriaComponenteComponente = idCategoriaComponenteComponente;
    }

    public ArrayList<String> getTest() {
        return test;
    }

    public void setTest(ArrayList<String> test) {
        this.test = test;
    }

  
    
}

