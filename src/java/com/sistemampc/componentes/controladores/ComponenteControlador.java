/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.componentes.controladores;

import com.sistemampc.modulos.facades.ComponenteFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import com.sistemampc.modelos.Componente;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Brianeto
 */
@Named(value = "componenteControlador")
@RequestScoped
public class ComponenteControlador implements Serializable, IControlador<Componente> {
    
    private static final long serialVersionUID = 2L;
    
    private Componente componente;
    @EJB private ComponenteFacade componenteFacade;

    public Componente getComponente() {
        return componente;
    }

    public void setComponente(Componente componente) {
        this.componente = componente;
    }

    public ComponenteFacade getComponenteFacade() {
        return componenteFacade;
    }
    
    public ComponenteControlador() {
    }
    
    @PostConstruct
    public void init(){
        componente = new Componente();
    }

    @Override
    public Componente getObjectoEntidad(Integer i) {
        return this.getComponenteFacade().find(i);
    }
    
    public List<Componente> listaComponentes(){
        return getComponenteFacade().findAll();
    }
    
}
