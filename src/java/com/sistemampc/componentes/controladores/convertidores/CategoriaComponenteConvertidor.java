/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.componentes.controladores.convertidores;

import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import com.sistemampc.modelos.CategoriaComponente;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Brianeto
 */
@FacesConverter(forClass = CategoriaComponente.class)
public class CategoriaComponenteConvertidor extends AbstractConvertidor{

    public CategoriaComponenteConvertidor() {
        this.nombreControlador = "categoriaComponenteControlador";
    }
    
}
