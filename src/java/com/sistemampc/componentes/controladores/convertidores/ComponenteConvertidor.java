/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.componentes.controladores.convertidores;

import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import com.sistemampc.modelos.Componente;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Brianeto
 */
@FacesConverter(forClass = Componente.class)
public class ComponenteConvertidor extends AbstractConvertidor{

    public ComponenteConvertidor() {
        this.nombreControlador = "componenteControlador";
    }
    
}
