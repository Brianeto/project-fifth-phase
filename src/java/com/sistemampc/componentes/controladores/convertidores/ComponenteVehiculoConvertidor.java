/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.componentes.controladores.convertidores;

import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import com.sistemampc.modelos.ComponenteVehiculo;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Brianeto
 */
@FacesConverter(forClass = ComponenteVehiculo.class)
public class ComponenteVehiculoConvertidor extends AbstractConvertidor{

    public ComponenteVehiculoConvertidor() {
        this.nombreControlador = "componenteVehiculoControlador";
    }
    
}
