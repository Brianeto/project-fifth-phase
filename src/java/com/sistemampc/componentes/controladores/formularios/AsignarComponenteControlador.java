    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.componentes.controladores.formularios;

import com.sistemampc.modelos.ComponenteVehiculo;
import com.sistemampc.modulos.facades.ComponenteVehiculoFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Johaneto
 */
@Named(value = "asignarComponenteControlador")
@RequestScoped
public class AsignarComponenteControlador implements Serializable, IControlador<ComponenteVehiculo> {

    /**
     * Creates a new instance of AsignarComponenteControlador
     */
    public AsignarComponenteControlador() {
    }
     private ComponenteVehiculo componenteVehiculo;
    @EJB private ComponenteVehiculoFacade componenteVehiculoFacade;

    public ComponenteVehiculo getComponenteVehiculo() {
        return componenteVehiculo;
    }

    public void setComponente(ComponenteVehiculo componenteVehiculo) {
        this.componenteVehiculo = componenteVehiculo;
    }

    public ComponenteVehiculoFacade getComponenteVehiculoFacade() {
        return componenteVehiculoFacade;
    }
    
    @PostConstruct
    public void init(){
        componenteVehiculo = new ComponenteVehiculo();
    }

    @Override
    public ComponenteVehiculo getObjectoEntidad(Integer i) {
        return this.getComponenteVehiculoFacade().find(i);
    }
    
    public void asignarComponenteVehiculo() {
        
     
    }
}
