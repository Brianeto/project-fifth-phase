/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.componentes.controladores.formularios;

import com.sistemampc.modelos.Componente;
import com.sistemampc.modulos.facades.ComponenteFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Johaneto
 */
@Named(value = "registrarComponenteControlador")
@RequestScoped
public class RegistrarComponenteControlador implements Serializable {

    /**
     * Creates a new instance of RegistrarComponenteControlador
     */
    private Componente componente;
    @EJB
    private ComponenteFacade componenteFacade;

    public Componente getComponente() {
        return componente;
    }

    public void setComponente(Componente componente) {
        this.componente = componente;
    }
    public RegistrarComponenteControlador() {
    }

    @PostConstruct
    public void init() {
        componente = new Componente();
    }

    public void registrarComponente() {
        componenteFacade.create(componente);
    }
}