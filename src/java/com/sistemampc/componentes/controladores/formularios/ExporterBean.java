/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.componentes.controladores.formularios;

import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Johaneto
 */
@Named(value = "exporterBean")
@ApplicationScoped
public class ExporterBean {
    public void export(List<?> data) {
        //create the Excel file on the fly
        //use Apache POI or another library to do it
        //use the data in List<?> data to export the relevant data

        //fire a file download...
    }
    
}
