/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.componentes.controladores;

import com.sistemampc.modulos.facades.ComponenteVehiculoFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import com.sistemampc.modelos.ComponenteVehiculo;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Brianeto
 */
@Named(value = "componenteVehiculoControlador")
@RequestScoped
public class ComponenteVehiculoControlador implements Serializable, IControlador<ComponenteVehiculo> {
    
    private static final long serialVersionUID = 2L;
    
    private ComponenteVehiculo componenteVehiculo;
    @EJB private ComponenteVehiculoFacade componenteVehiculoFacade;

    public ComponenteVehiculo getComponenteVehiculo() {
        return componenteVehiculo;
    }

    public void setComponenteVehiculo(ComponenteVehiculo componenteVehiculo) {
        this.componenteVehiculo = componenteVehiculo;
    }

    public ComponenteVehiculoFacade getComponenteVehiculoFacade() {
        return componenteVehiculoFacade;
    }
    
    public ComponenteVehiculoControlador() {
    }
    
    @PostConstruct
    public void init(){
        componenteVehiculo = new ComponenteVehiculo();
    }

    @Override
    public ComponenteVehiculo getObjectoEntidad(Integer i) {
        return this.getComponenteVehiculoFacade().find(i);
    }
    
    public List<ComponenteVehiculo> listaComponentesVehiculos(){
        return getComponenteVehiculoFacade().findAll();
    }
    
    
}
