/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.vehiculos.bussines;

import com.sistemampc.modelos.Vehiculo;
import com.sistemampc.modulos.utilidades.UploadFIleUtils;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.Part;

/**
 *
 * @author APRENDIZ
 */
@Stateless
public class VehiculoSesionBean {

    @PersistenceContext(unitName = "sistemampcPU")
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }

    public boolean existeChasis(String chasis) {
        try {
            return getEntityManager().createNamedQuery("Vehiculo.findByNumeroChasis", Vehiculo.class).setParameter("numeroChasis", chasis).getSingleResult() != null;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean existeMatricula(String matricula) {
        try {
            return getEntityManager().createNamedQuery("Vehiculo.findByMatricula", Vehiculo.class).setParameter("matricula", matricula).getSingleResult() != null;
        } catch (Exception e) {
            return false;
        }
    }

    public Object registrarVehiculo(Vehiculo vehiculo, Part file) {
        try {
            if (!existeMatricula(vehiculo.getMatricula())) {
                if (!existeChasis(vehiculo.getNumeroChasis())) {
                    vehiculo.setEstadoVehiculo((short) 5);
                    vehiculo.setMatricula(vehiculo.getMatricula().toUpperCase());
                    vehiculo.setNumeroChasis(vehiculo.getNumeroChasis().toUpperCase());
                    getEntityManager().persist(vehiculo);
                    if (file != null) {
                        Vehiculo v = getEntityManager().createNamedQuery("Vehiculo.findByMatricula", Vehiculo.class)
                                .setParameter("matricula", vehiculo.getMatricula()).getSingleResult();
                        v.setFotoVehiculo(UploadFIleUtils.upload(file));
                        getEntityManager().merge(v);
                        return v;
                    }
                    return vehiculo;
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } catch (Exception e) {
            return 1;
        }
    }

}
