/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.vehiculos.bussines;

import com.sistemampc.modelos.ComponenteVehiculo;
import com.sistemampc.modelos.Vehiculo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author APRENDIZ
 */
@Stateless
public class FichaTecnicaSessionBean {
    @PersistenceContext(unitName = "sistemampcPU")
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }
    
    public List<ComponenteVehiculo> listaComponenteVehiculo(Vehiculo vehiculo){
        try {
            return getEntityManager().createNamedQuery("ComponenteVehiculo.findByIdVehiculo", ComponenteVehiculo.class)
                    .setParameter("idvehiculo", vehiculo).getResultList();
        } catch (Exception e) {
            System.out.println("error" +  e.getMessage());
            return null;
        }
    }
    
}
