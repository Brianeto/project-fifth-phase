package com.sistemampc.vehiculos.controladores;

import com.sistemampc.modelos.Vehiculo;
import com.sistemampc.modulos.facades.VehiculoFacade;
import com.sistemampc.modulos.utilidades.FacesUtils;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author APRENDIZ
 */
@Named
@RequestScoped
public class VehiculoControlador implements Serializable, IControlador<Vehiculo>{
    
    private static final long serialVersionUID = 1L;
    private Vehiculo vehiculo;
    
    @EJB
    private VehiculoFacade vehiculoFacade;
    
    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    @PostConstruct
    public void init(){
        vehiculo = new Vehiculo();
    }
    
    public List<Vehiculo> listaVehiculos(){
        return vehiculoFacade.findAll();
    }

    @Override
    public Vehiculo getObjectoEntidad(Integer i) {
        return vehiculoFacade.find(i);
    }
    
    public String capturaVehiculo(Vehiculo vehiculo){
        FacesUtils.setObjectSession("vehiculo", vehiculo);
        return "/protegido/vehiculo/ficha/fichatecnica.mpc?faces-redirect=true";
    }
}