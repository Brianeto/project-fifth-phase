/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.vehiculos.controladores;

import com.sistemampc.modelos.FallaVehiculo;
import com.sistemampc.modulos.facades.FallaVehiculoFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author APRENDIZ
 */
@Named
@RequestScoped
public class FallaVehiculoControlador implements Serializable, IControlador<FallaVehiculo>{
    
    private static final long serialVersionUID = 1L;
    
    private FallaVehiculo fallaVehiculo;
    @EJB
    private FallaVehiculoFacade fallaVehiculoFacade;

    public FallaVehiculo getFallaVehiculo() {
        return fallaVehiculo;
    }

    public void setFallaVehiculo(FallaVehiculo fallaVehiculo) {
        this.fallaVehiculo = fallaVehiculo;
    }
    
    @PostConstruct
    public void init(){
        fallaVehiculo = new FallaVehiculo();
    }
    
    public List<FallaVehiculo> listaFallasVehiculos(){
        return fallaVehiculoFacade.findAll();
    }

    @Override
    public FallaVehiculo getObjectoEntidad(Integer i) {
        return fallaVehiculoFacade.find(i);
    }
}
