/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.vehiculos.controladores;

import com.sistemampc.modelos.DocumentoVehiculo;
import com.sistemampc.modulos.facades.DocumentoVehiculoFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author APRENDIZ
 */
@Named
@RequestScoped
public class DocumentoVehiculoControlador implements Serializable, IControlador<DocumentoVehiculo>{

    private DocumentoVehiculo documentoVehiculo;
    @EJB
    private DocumentoVehiculoFacade documentoVehiculoFacade;

    public DocumentoVehiculo getDocumentoVehiculo() {
        return documentoVehiculo;
    }

    public void setDocumentoVehiculo(DocumentoVehiculo documentoVehiculo) {
        this.documentoVehiculo = documentoVehiculo;
    }

    public DocumentoVehiculoControlador() {
    }
    
    @PostConstruct
    public void init(){
        documentoVehiculo = new DocumentoVehiculo();
    }
    
    public List<DocumentoVehiculo> listaDocumentosVehiculos(){
        return documentoVehiculoFacade.findAll();
    }
    
    @Override
    public DocumentoVehiculo getObjectoEntidad(Integer i) {
        return documentoVehiculoFacade.find(i);
    }
    
    
}