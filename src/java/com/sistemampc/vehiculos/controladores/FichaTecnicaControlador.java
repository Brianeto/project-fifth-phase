/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.vehiculos.controladores;

import com.sistemampc.modelos.ComponenteVehiculo;
import com.sistemampc.modelos.Orden;
import com.sistemampc.modelos.Vehiculo;
import com.sistemampc.modulos.facades.VehiculoFacade;
import com.sistemampc.modulos.utilidades.FacesUtils;
import com.sistemampc.vehiculos.bussines.FichaTecnicaSessionBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author APRENDIZ
 */
@Named(value = "fichaTecnicaControlador")
@RequestScoped
public class FichaTecnicaControlador implements Serializable {

    private Vehiculo vehiculoFicha;
    @EJB
    private FichaTecnicaSessionBean fichaTecnicaSessionBean;
    @EJB
    private VehiculoFacade vf;

    public Vehiculo getVehiculoFicha() {
        return vehiculoFicha;
    }

    public FichaTecnicaControlador() {
    }

    @PostConstruct
    public void init() {
        vehiculoFicha = (Vehiculo) FacesUtils.getObjectSession("vehiculo");
    }

    public List<ComponenteVehiculo> listaComponentesVehiculo() {
        try {
            return fichaTecnicaSessionBean.listaComponenteVehiculo((Vehiculo) FacesUtils.getObjectSession("vehiculo"));
        } catch (Exception e) {
            return null;
        }
    }

    public void cambairEstadoVehiculo() {
        vehiculoFicha.setEstadoVehiculo((short) 5);
        vf.edit(vehiculoFicha);
        FacesUtils.setObjectSession("vehiculo", vf);
    }

    public List<Orden> listarOrdenVehiculo() {
        return vehiculoFicha.getOrdenList();
    }

    public List<Orden> listarOrdenesPorTipoPorEstado(short i) {
        List<Orden> lista = new ArrayList<>();
        for (Orden o : listarOrdenVehiculo()) {
            if (o.getEstadoOrden() == i) {
                lista.add(o);
            }
        }
        return lista;
    }

    public List<Orden> listarOrdenesPorTipoPorEstado(short a, short b) {
        List<Orden> lista = new ArrayList<>();
        for (Orden o : listarOrdenVehiculo()) {
            if (o.getEstadoOrden() == a|| o.getEstadoOrden()==b) {
                lista.add(o);
            }
        }
        return lista;
    }

}
