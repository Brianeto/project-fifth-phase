/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.vehiculos.controladores.convertidores;

import com.sistemampc.modelos.TipoVehiculo;
import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author APRENDIZ
 */
@FacesConverter( forClass = TipoVehiculo.class)
public class TipoVehiculoConvertidor extends AbstractConvertidor {

    public TipoVehiculoConvertidor() {
        this.nombreControlador = "tipoVehiculoControlador";
    }
    
}
