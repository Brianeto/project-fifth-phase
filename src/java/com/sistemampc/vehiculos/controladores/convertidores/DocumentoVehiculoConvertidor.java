/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.vehiculos.controladores.convertidores;

import com.sistemampc.modelos.DocumentoVehiculo;
import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author APRENDIZ
 */
@FacesConverter (forClass = DocumentoVehiculo.class)
public class DocumentoVehiculoConvertidor extends AbstractConvertidor{

    public DocumentoVehiculoConvertidor() {
        this.nombreControlador = "documentoVehiculo";
    }
    
}
