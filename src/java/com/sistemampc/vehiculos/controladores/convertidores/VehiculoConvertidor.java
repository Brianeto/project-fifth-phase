/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.vehiculos.controladores.convertidores;

import com.sistemampc.modelos.Vehiculo;
import com.sistemampc.modulos.utilidades.AbstractConvertidor;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author APRENDIZ
 */
@FacesConverter( forClass = Vehiculo.class)
public class VehiculoConvertidor extends AbstractConvertidor {

    public VehiculoConvertidor() {
        this.nombreControlador = "vehiculoControlador";
    }
    
}
