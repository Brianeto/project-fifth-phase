/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.vehiculos.controladores.formularios;

import com.sistemampc.modelos.Ciudad;
import com.sistemampc.modelos.Usuario;
import com.sistemampc.modelos.Vehiculo;
import com.sistemampc.modulos.utilidades.FacesUtils;
import com.sistemampc.modulos.utilidades.UploadFIleUtils;
import com.sistemampc.vehiculos.bussines.VehiculoSesionBean;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.servlet.http.Part;

/**
 *
 * @author Johaneto
 */
@Named(value = "registrarVehiculoControlador")
@RequestScoped
public class RegistrarVehiculoControlador implements Serializable {

    private Vehiculo vehiculo;
    private Part file;
    @EJB
    private VehiculoSesionBean vehiculoSesionBean;

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }
    
    

    public RegistrarVehiculoControlador() {
    }

    @PostConstruct
    public void init() {
        vehiculo = new Vehiculo();
    }

    public String registrarVehiculo() {
        try {
            Usuario u = (Usuario) FacesUtils.getObjectSession("usuario");
            vehiculo.setIdCiudad(u.getIdCiudad());
            Object objeto = vehiculoSesionBean.registrarVehiculo(vehiculo, file);
            if(objeto instanceof Integer){
                switch((Integer) objeto){
                    case 1:
                        FacesUtils.showFacesMessage(null, "Error", "ocurrio un error", 1);
                        break;
                    case 2:
                        FacesUtils.showFacesMessage(null, "Matricula", "ya se encuentra registrada", 1);
                        break;
                    case 3:
                        FacesUtils.showFacesMessage(null, "Número chasis", "ya se ecntra registrado", 1);
                        break;
                }
            }else if(objeto instanceof Vehiculo){
                FacesUtils.setObjectSession("vehiculo", (Vehiculo) objeto);
                vehiculo.setFotoVehiculo(UploadFIleUtils.upload(file));
                return "/protegido/vehiculo/ficha/fichatecnica.mpc?faces-redirect=true";
            }
        } catch (Exception e) {
            FacesUtils.showFacesMessage(null, "Error", "ocurrio un error en el controlador", 1);
        }
        return null;
    }
}

