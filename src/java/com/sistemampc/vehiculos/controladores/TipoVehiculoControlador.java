/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistemampc.vehiculos.controladores;

import com.sistemampc.modelos.TipoVehiculo;
import com.sistemampc.modulos.facades.TipoVehiculoFacade;
import com.sistemampc.modulos.utilidades.IControlador;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author APRENDIZ
 */
@Named(value = "tipoVehiculoControlador")
@RequestScoped
public class TipoVehiculoControlador implements Serializable, IControlador<TipoVehiculo>{

    private static final long serialVersionUID = 3L;
    
    private TipoVehiculo tipoVehiculo;
    @EJB
    private TipoVehiculoFacade tipoVehiculoFacade;

    public TipoVehiculo getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(TipoVehiculo tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }
   
    public TipoVehiculoControlador() {
    }
    
    @PostConstruct
    public void init(){
        tipoVehiculo = new TipoVehiculo();
    }
    
    @Override
    public TipoVehiculo getObjectoEntidad(Integer i) {
        return tipoVehiculoFacade.find(i);
    }
    
    public List<TipoVehiculo> listaTipoVehiculos(){
        return tipoVehiculoFacade.findAll();
    }
}
